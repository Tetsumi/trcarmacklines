trcarmacklines
===================

A toy app to play with Carmack's line and more.

![Demo (Webm video)](https://webm.red/bfYG.webm)


How to run
-------------------

Execute `racket ./source/main.rkt`

[Note: I am no longer working on this.]
